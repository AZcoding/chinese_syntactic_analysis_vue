import type { AppRouteModule } from '@/router/types';

import { LAYOUT } from '@/router/constant';
import { t } from '@/hooks/web/useI18n';
import { getUserInfo } from '@/api/sys/user';

let role_id = 0;

await getUserInfo().then((res)=>{
  role_id = res.role_id;
}).catch((err)=>{
console.log(err);
})
console.log("角色ID",role_id);

const charts: AppRouteModule = {
  path: '/analysis_center',
  name: 'analysis_center',
  component: LAYOUT,
  redirect: '/analysis_center/analysis_list',
  meta: {
    orderNo: 12,
    icon: 'ant-design:bar-chart-outlined',
    title: t('routes.demo.analysis_center.page'),
  },
  children: getAnalysisCenterRouter(),
};


function getAnalysisCenterRouter() {
  let list = [
    {
      path: 'analysis_list',
      name: 'analysis_list',
      meta: {
        title: t('routes.demo.analysis_center.analysis_list'),
      },
      component: () => import('@/views/analysis_center/analysis_list/index.vue'),
    },
  ]

  // 管理员和研究者可以查看具体的分析数据
  if (role_id == 1 || role_id == 2) {

    list.push({
      path: 'analysis_data',
      name: 'analysis_data',
      meta: {
        title: t('routes.demo.analysis_center.analysis_data'),
      },
      component: () => import('@/views/analysis_center/analysis_data/index.vue'),
    })

    // list.push({
    //   path: 'user_analysis',
    //   name: 'user_analysis',
    //   meta: {
    //     title: t('routes.demo.analysis_center.user_analysis'),
    //   },
    //   component: () => import('@/views/analysis_center/user_analysis/index.vue'),
    // })

    list.push({
      path: 'syntax_level',
      name: 'syntax_level',
      meta: {
        title: '句法等级',
      },
      component: () => import('@/views/analysis_center/syntax_level/index.vue'),
    })

  } // 管理员和研究者可以查看具体的分析数据

  return list;
}

export default charts;
