import type { AppRouteModule } from '@/router/types';

// import { LAYOUT } from '@/router/constant';
import { t } from '@/hooks/web/useI18n';

import { getParentLayout, LAYOUT } from '@/router/constant';
import { RoleEnum } from '@/enums/roleEnum';

const system: AppRouteModule = {
  path: '/system',
  name: 'System',
  component: LAYOUT,
  redirect: '/system/account',
  meta: {
    orderNo: 14,
    icon: 'ion:settings-outline',
    title: t('routes.demo.system.moduleName'),
  },
  children: [
    {
      path: 'account',
      name: 'AccountManagement',
      meta: {
        icon: "ant-design:usergroup-add-outlined",
        title: "用户管理",//t('routes.demo.system.account'),
        ignoreKeepAlive: false,
      },
      component: () => import('@/views/demo/system/account/index.vue'),
    },
    // {
    //   path: 'vxeTableAccount',
    //   name: 'VxeTableAccountManagement',
    //   meta: {
    //     title: t('routes.demo.system.vxeTableAccount'),
    //     ignoreKeepAlive: false,
    //   },
    //   component: () => import('@/views/demo/system/vxe-account/index.vue'),
    // },
    {
      path: 'account_detail/:id',
      name: 'AccountDetail',
      meta: {
        hideMenu: true,
        title: t('routes.demo.system.account_detail'),
        ignoreKeepAlive: true,
        showMenu: false,
        currentActiveMenu: '/system/account',
      },
      component: () => import('@/views/demo/system/account/AccountDetail.vue'),
    },
    {
      path: 'role',
      name: 'RoleManagement',
      meta: {
        icon: "ant-design:solution-outlined",
        title: t('routes.demo.system.role'),
        ignoreKeepAlive: true,
      },
      component: () => import('@/views/demo/system/role/index.vue'),
    },

    // {
    //   path: 'menu',
    //   name: 'MenuManagement',
    //   meta: {
    //     title: t('routes.demo.system.menu'),
    //     ignoreKeepAlive: true,
    //   },
    //   component: () => import('@/views/demo/system/menu/index.vue'),
    // },

    {
      path: '/permission',
      name: 'Permission',
      // redirect: '/permission/front/page',
      meta: {
        orderNo: 15,
        icon: 'ion:key-outline',
        title: '权限管理',
      },
      component: () => import('@/views/demo/system/permission/index.vue'),
    },
    // {
    //   path: 'dept',
    //   name: 'DeptManagement',
    //   meta: {
    //     title: t('routes.demo.system.dept'),
    //     ignoreKeepAlive: true,
    //   },
    //   component: () => import('@/views/demo/system/dept/index.vue'),
    // },
    // {
    //   path: 'changePassword',
    //   name: 'ChangePassword',
    //   meta: {
    //     title: t('routes.demo.system.password'),
    //     ignoreKeepAlive: true,
    //   },
    //   component: () => import('@/views/demo/system/password/index.vue'),
    // },
    {
      path: 'logs',
      name: 'logs',
      meta: {
        icon: "ant-design:file-search-outlined",
        title: '系统日志',
        ignoreKeepAlive: true,
      },
      component: () => import('@/views/demo/system/logs/index.vue'),
    },
  ],
};

export default system;
