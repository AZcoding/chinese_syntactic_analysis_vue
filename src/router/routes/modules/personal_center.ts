import type { AppRouteModule } from '@/router/types';

import { LAYOUT } from '@/router/constant';
import { t } from '@/hooks/web/useI18n';

const charts: AppRouteModule = {
  path: '/personal_center',
  name: 'personal_center',
  component: LAYOUT,
  redirect: '/personal_center/index',
  meta: {
    orderNo: 13,
    icon: 'ant-design:user-outlined',
    title: '个人中心',
  },
  children: [
    {
      path: 'index',
      name: 'personal_info',
      meta: {
        title: '我的信息',
      },
      component: () => import('@/views/personal_center/user_info/index.vue'),
    },
    {
      path: 'reset_password',
      name: 'reset_password',
      meta: {
        title: '重置密码',
      },
      component: () => import('@/views/personal_center/reset_password/index.vue'),
    },
  ],
};

export default charts;
