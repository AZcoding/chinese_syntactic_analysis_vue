import type { AppRouteModule } from '@/router/types';

import { LAYOUT } from '@/router/constant';
import { t } from '@/hooks/web/useI18n';

const steps: AppRouteModule = {
  path: '/analysis_editor',
  name: 'Editor',
  component: LAYOUT,
  redirect: '/analysis_editor/index',
  meta: {
    orderNo: 11,
    hideChildrenInMenu: true,
    icon: 'tabler:chart-dots',
    title: t('routes.demo.analysis_editor.page'),
  },
  children: [
    {
      path: 'index',
      name: 'EditorPage',
      component: () => import('@/views/analysis_editor/index.vue'),
      meta: {
        title: t('routes.demo.analysis_editor.page'),
        icon: 'tabler:chart-dots',
        hideMenu: true,
      },
    },
    {
      path: 'success',
      name: 'success',
      component: () => import('@/views/analysis_editor/SuccessPage.vue'),
      meta: {
        title: '分析提交成功',
        icon: 'tabler:chart-dots',
        hideMenu: true,
      },
    },
  ],
};

export default steps;
