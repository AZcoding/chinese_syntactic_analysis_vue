import { getRoleListByPage, isAccountExist } from '@/api/demo/system';
import { BasicColumn, FormSchema } from '@/components/Table';
import { h,ref } from 'vue';
import { Switch } from 'ant-design-vue';
import { setUserStatus } from '@/api/demo/system';
import { useMessage } from '@/hooks/web/useMessage';

/**
 * transform mock data
 * {
 *  0: '华东分部',
 * '0-0': '华东分部-研发部'
 * '0-1': '华东分部-市场部',
 *  ...
 * }
 */

export const deptMap = (() => {
  const pDept = ['华东分部', '华南分部', '西北分部'];
  const cDept = ['研发部', '市场部', '商务部', '财务部'];

  return pDept.reduce((map, p, pIdx) => {
    map[pIdx] = p;

    cDept.forEach((c, cIndex) => (map[`${pIdx}-${cIndex}`] = `${p}-${c}`));

    return map;
  }, {});
})();

const sexMap = new Map([

  [1, '男'],
  [2, '女'],
  [3, '未知'],
  [0, '未知'],
]);

export const refreshTable = ref(false);

type CheckedType = boolean | string | number;

export const columns: BasicColumn[] = [
  {
    title: '用户ID',
    dataIndex: 'user_id',
    width: 150,
  },
  {
    title: '用户名',
    dataIndex: 'username',
    width: 120,
  },
  {
    title: '角色',
    dataIndex: 'role',
    width: 120,
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 120,
    customRender: ({ record }) => {
      if(record.role_id == 1 || record.role_id == 2 || record.role_id == 3) {
        return "-"   
      }
      // let system_role_id = [1,2,3];
      // system_role_id.find(function(value,index,system_role_id){
      //   if(value== record.role_id) {
      //     return "-"   
      //   }
      // })

      if (!Reflect.has(record, 'pendingStatus')) {
        record.pendingStatus = false;
      }
      return h(Switch, {
        checked: record.status === 1,
        checkedChildren: '停用',
        unCheckedChildren: '启用',
        loading: record.pendingStatus,
        onChange(checked: CheckedType) {
          record.pendingStatus = true;
          const newStatus = checked ? 1 : 0;
          const { createMessage } = useMessage();
          setUserStatus(record.user_id, newStatus)
            .then(() => {
              record.status = newStatus;
              createMessage.success(`已成功修改用户状态`);
            })
            .catch(() => {
              createMessage.error('修改用户状态失败');
            })
            .finally(() => {
              record.pendingStatus = false;
            });
        },
      });
    },
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    width: 180,
  },
  {
    title: '性别',
    dataIndex: 'sex',
    width: 80,
    customRender: ({value}) => {
      return sexMap.get(value);
    }
  },
  {
    title: '年龄',
    dataIndex: 'age',
    width: 80,
  },
  {
    title: '注册时间',
    dataIndex: 'register_at',
    width: 180,
  },
  // {
  //   title: '所属部门',
  //   dataIndex: 'dept',
  //   customRender: ({ value }) => {
  //     return deptMap[value];
  //   },
  // },
  // {
  //   title: '备注',
  //   dataIndex: 'remark',
  // },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'user_id',
    label: '用户ID',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'username',
    label: '用户名',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '全部', value: -1 },
        { label: '正常', value: 1 },
        { label: '失效', value: 0 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const selectRoleId = ref(0);

export const accountFormSchema: FormSchema[] = [
  {
    field: 'username',
    label: '用户名',
    component: 'Input',
    helpMessage: ['请输入新的用户名'],
    rules: [
      {
        required: true,
        message: '请输入用户名',
      },
      // {
      //   trigger: 'blur',
      //   validator(_, value) {
      //     return new Promise((resolve, reject) => {
      //       // if (!value) return resolve();
      //       // isAccountExist(value)
      //       //   .then(resolve)
      //       //   .catch((err) => {
      //       //     reject(err.message || '验证失败');
      //       //   });
      //     });
      //   },
      // },
    ],
  },
  {
    field: 'avatar',
    label: '头像（地址）',
    component: 'Input',
    required: false,
  },
  {
    field: 'age',
    label: '年龄',
    component: 'Input',
    required: true,
  },
  {
    field: 'sex',
    label: '性别',
    component: 'Select',
    componentProps: {
      options: [
        { label: '男', value: 1 },
        { label: '女', value: 2 },
        { label: '未知', value: 3 },
      ],
    },    
    required: true,
  },

  // {
  //   field: 'pwd',
  //   label: '密码',
  //   component: 'InputPassword',

  //   required: true,
  //   ifShow: false,
  // },
  // {
  //   label: '角色',
  //   field: 'role_id',
  //   component: 'ApiSelect',
  //   componentProps: {
  //     api: getRoleListByPage,
  //     params: {page_index:1,page_size:9999,status:-1},
  //     resultField: 'items',
  //     labelField: 'role_name',
  //     valueField: 'role_id',
  //     onChange: (e, v) => {
  //       // console.log('ApiSelect====>:', e, v);
  //       selectRoleId.value = Number(e);
  //       // console.log("select",selectRoleId.value)
  //     },
  //   },
  //   required: true,
  // },
  // {
  //   field: 'dept',
  //   label: '所属部门',
  //   component: 'TreeSelect',
  //   componentProps: {
  //     fieldNames: {
  //       label: 'deptName',
  //       key: 'id',
  //       value: 'id',
  //     },
  //     getPopupContainer: () => document.body,
  //   },
  //   required: true,
  // },
  // {
  //   field: 'nickname',
  //   label: '昵称',
  //   component: 'Input',
  //   required: true,
  // },

  // {
  //   label: '邮箱',
  //   field: 'email',
  //   component: 'Input',
  //   required: true,
  // },

  // {
  //   label: '备注',
  //   field: 'remark',
  //   component: 'InputTextArea',
  // },
];
