import { BasicColumn, FormSchema } from '@/components/Table';
import { h } from 'vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatus,getDimension } from '@/api/demo/system';
import { useMessage } from '@/hooks/web/useMessage';
import { formatToDateTime } from '@/utils/dateUtil';

let dimension_map = new Map();

await getDimension().then((res) => {
  res.forEach((item) => {
    // console.log("item",item);
    dimension_map.set(item.id,item.name);
  })
});

type CheckedType = boolean | string | number;
export const columns: BasicColumn[] = [
  {
    title: '规则ID',
    dataIndex: 'id',
    width: 120,
    ifShow: false,
  },
  {
    title: '规则类型',
    dataIndex: 'analysis_type',
    width: 100,
    customRender: ({ value }) => {
      if (dimension_map.get(value) != undefined){
        return dimension_map.get(value);
      }
      return '测试类型';
    },
  },
  {
    title: '最低分数',
    dataIndex: 'min_score',
    width: 100,
  },
  {
    title: '最高分数',
    dataIndex: 'max_score',
    width: 100,
  },
  {
    title: '备注',
    dataIndex: 'comment',
    width: 160,
  },
  // {
  //   title: '创建时间',
  //   dataIndex: 'created_at',
  //   width: 180,
  //   customRender: ({ value }) => {
  //     return formatToDateTime(new Date(value * 1000));
  //   },
  // },
  // {
  //   title: '修改时间',
  //   dataIndex: 'updated_at',
  //   width: 180,
  //   customRender: ({ value }) => {
  //     if (value == 0) {
  //       return "-"
  //     }
  //     return formatToDateTime(new Date(value * 1000));
  //   },
  // },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'permission_name',
    label: '权限名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'permission_value',
    label: '权限内容',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'comment',
    label: '备注',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'permission_type',
    label: '权限类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '全部', value: 0 },
        { label: '前端', value: 1 },
        { label: '后端', value: 2 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'permission_id',
    label: '权限ID',
    // required: true,
    component: 'Input',
    show: false,
  },
  {
    field: 'permission_name',
    label: '权限名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'permission_type',
    label: '权限类型',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '前端路由', value: 1 },
        { label: '后端接口', value: 2 },
      ],
    },
  },
  {
    field: 'permission_value',
    label: '权限内容',
    required: true,
    component: 'Input',
  },
  {
    label: '备注',
    field: 'comment',
    component: 'InputTextArea',
  },
  // {
  //   label: ' ',
  //   field: 'menu',
  //   slot: 'menu',
  // },
];
