import { BasicColumn, FormSchema } from '@/components/Table';
import { h } from 'vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatus } from '@/api/demo/system';
import { useMessage } from '@/hooks/web/useMessage';
import { formatToDateTime } from '@/utils/dateUtil';

type CheckedType = boolean | string | number;
export const columns: BasicColumn[] = [
  {
    title: '角色ID',
    dataIndex: 'role_id',
    width: 180,
    ifShow: false,
  },
  {
    title: '角色名称',
    dataIndex: 'role_name',
    width: 180,
  },
  {
    title: '备注',
    dataIndex: 'comment',
    width: 180,
    customRender: ({ record }) => {
      if(record.role_id == 1 || record.role_id == 2 || record.role_id == 3) {
        return "系统内置角色"   
      }
    },
  },
  {
    title: '创建时间',
    dataIndex: 'created_at',
    width: 180,
    customRender: ({ value }) => {
      return formatToDateTime(new Date(value * 1000));
    },
  },
  {
    title: '修改时间',
    dataIndex: 'updated_at',
    width: 180,
    customRender: ({ value }) => {
      if (value == 0) {
        return "-"
      }
      return formatToDateTime(new Date(value * 1000));
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 120,
    customRender: ({ record }) => {
      if(record.role_id == 1 || record.role_id == 2 || record.role_id == 3) {
        return "-"   
      }
      // let system_role_id = [1,2,3];
      // system_role_id.find(function(value,index,system_role_id){
      //   if(value== record.role_id) {
      //     return "-"   
      //   }
      // })
      if (!Reflect.has(record, 'pendingStatus')) {
        record.pendingStatus = false;
      }
      return h(Switch, {
        checked: record.status === 1,
        checkedChildren: '停用',
        unCheckedChildren: '启用',
        loading: record.pendingStatus,
        onChange(checked: CheckedType) {
          record.pendingStatus = true;
          const newStatus = checked ? 1 : 0;
          const { createMessage } = useMessage();
          setRoleStatus(record.role_id, newStatus)
            .then(() => {
              record.status = newStatus;
              createMessage.success(`已成功修改角色状态`);
            })
            .catch(() => {
              createMessage.error('修改角色状态失败');
            })
            .finally(() => {
              record.pendingStatus = false;
            });
        },
      });
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'role_name',
    label: '角色名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
        { label: '全部', value: -1 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'role_id',
    label: '角色ID',
    // required: true,
    component: 'Input',
    show: false,
  },
  {
    field: 'role_name',
    label: '角色名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'status',
    label: '状态',
    component: 'RadioButtonGroup',
    defaultValue: 0,
    componentProps: {
      options: [
        { label: '启用', value: 1 },
        { label: '停用', value: 0 },
      ],
    },
  },
  {
    label: '备注',
    field: 'comment',
    component: 'InputTextArea',
  },
  // {
  //   label: ' ',
  //   field: 'menu',
  //   slot: 'menu',
  // },
];
