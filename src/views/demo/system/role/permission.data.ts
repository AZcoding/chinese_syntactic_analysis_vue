import { BasicColumn, FormSchema } from '@/components/Table';
import { h } from 'vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatus } from '@/api/demo/system';
import { useMessage } from '@/hooks/web/useMessage';
import { formatToDateTime } from '@/utils/dateUtil';

type CheckedType = boolean | string | number;
export const columns: BasicColumn[] = [
  {
    title: '权限名称',
    dataIndex: 'permission_name',
    width: 120,
  },
  {
    title: '权限类型',
    dataIndex: 'permission_type',
    width: 100,
    customRender: ({ value }) => {
      if (value == 1) {
        return "前端路由";
      }
      if (value == 2) {
        return "后端接口";
      }
      return "未知类型";
    },
  },
  {
    title: '权限内容',
    dataIndex: 'permission_value',
    width: 180,
  },
  {
    title: '备注',
    dataIndex: 'comment',
    width: 160,
  },
  {
    title: '创建时间',
    dataIndex: 'created_at',
    width: 180,
    customRender: ({ value }) => {
      return formatToDateTime(new Date(value * 1000));
    },
  },
  {
    title: '修改时间',
    dataIndex: 'updated_at',
    width: 180,
    customRender: ({ value }) => {
      if (value == 0) {
        return "-"
      }
      return formatToDateTime(new Date(value * 1000));
    },
  },
  // {
  //   title: '状态',
  //   dataIndex: 'status',
  //   width: 120,
  //   customRender: ({ record }) => {
  //     if (!Reflect.has(record, 'pendingStatus')) {
  //       record.pendingStatus = false;
  //     }
  //     return h(Switch, {
  //       checked: record.status === '1',
  //       checkedChildren: '停用',
  //       unCheckedChildren: '启用',
  //       loading: record.pendingStatus,
  //       onChange(checked: CheckedType) {
  //         record.pendingStatus = true;
  //         const newStatus = checked ? '1' : '0';
  //         const { createMessage } = useMessage();
  //         setRoleStatus(record.id, newStatus)
  //           .then(() => {
  //             record.status = newStatus;
  //             createMessage.success(`已成功修改角色状态`);
  //           })
  //           .catch(() => {
  //             createMessage.error('修改角色状态失败');
  //           })
  //           .finally(() => {
  //             record.pendingStatus = false;
  //           });
  //       },
  //     });
  //   },
  // },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'permission_name',
    label: '权限名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'permission_value',
    label: '权限内容',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'comment',
    label: '备注',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'permission_type',
    label: '权限类型',
    component: 'Select',
    componentProps: {
      options: [
        { label: '全部', value: 0 },
        { label: '前端', value: 1 },
        { label: '后端', value: 2 },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'permission_id',
    label: '权限ID',
    // required: true,
    component: 'Input',
    show: false,
  },
  {
    field: 'permission_name',
    label: '权限名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'permission_type',
    label: '权限类型',
    component: 'RadioButtonGroup',
    defaultValue: 1,
    componentProps: {
      options: [
        { label: '前端路由', value: 1 },
        { label: '后端接口', value: 2 },
      ],
    },
  },
  {
    field: 'permission_value',
    label: '权限内容',
    required: true,
    component: 'Input',
  },
  {
    label: '备注',
    field: 'comment',
    component: 'InputTextArea',
  },
  // {
  //   label: ' ',
  //   field: 'menu',
  //   slot: 'menu',
  // },
];
