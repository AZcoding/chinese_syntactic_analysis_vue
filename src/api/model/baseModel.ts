export interface BasicPageParams {
  // page: number;
  // pageSize: number;
  page_index: number;
  page_size: number;
}

export interface BasicFetchResult<T> {
  // items: T[];
  // total: number;
  list: T[];
  count: number;
  page_index: number;
  page_size: number;
}

// 分析列表
export interface AnalysisPageParams {
  api_type: string;
  page_index: number;
  page_size: number;
  request_id: string;
  comment: string;
  text: string;
  user_id: number;
  begin_time: number;
  end_time: number;
}

// 分析统计
export interface AnalysisStatisticParams {
  top_word_limit: number;
}

// 分析状态
export interface AnalysisStatusParams {
  user_id: number;
  request_ids: string[];
}

export interface Data<T> {
  list: T[];
  count: number;
}

export interface AnalysisListResult<T> {
  data: Data<T>;
}
