import { AxiosHeaders } from 'axios';

import { TOKEN_KEY } from '@/enums/cacheEnum';
import { WebStorage } from '@/utils/cache/index';

export function getAuthHeader(): AxiosHeaders {
  const auth_header = new AxiosHeaders();
  auth_header.set('Access-Token', WebStorage.get(TOKEN_KEY));
  return auth_header;
}

export function getAuthToken(): string {
  return WebStorage.get(TOKEN_KEY);
}
