/**
 * @description: send sms parameters
 */
export interface SmsParams {
  email: string;
}

/**
 * @description: reset password parameters
 */
export interface ResetPasswordParams {
  email: string;
  identifying_code: string;
  new_password: string;
}

/**
 * set new password
*/
export interface SetNewPasswordParams {
  origin_password: string;
  new_password: string;
}

/***
 *  register
 */
export interface RegisterParams {
  username: string;
  password: string;
  email: string;
  identifying_code: string;
  sex: number;
  avatar: string;
  age: number;
}

/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  identity: string;
  password: string;
}

export interface UserInfo {
  user_id: number;
  username: string;
  role_name: string;
}

export interface Token {
  access_token: string;
  access_token_expire: number;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

/**
 * register response model
 */
export interface RegisterRespModel {
  user_info: UserInfo;
  token: Token;
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  user_info: UserInfo;
  token: Token;
  // roles: RoleInfo[];
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  // 用户id
  user_id: string | number;
  // 邮箱
  email : string;
  // 注册时间
  register_at: number;
  // 用户名
  username: string;
  // 角色ID
  role_id: number;
  // 角色名称
  role: string;
  // 头像
  avatar: string;
  // 年龄
  age: number;
  // 性别
  sex: number;

  // 真实名字
  realName: string;
  // 介绍
  desc?: string;
  // 角色名称
  roles: RoleInfo[];
}
