import { defHttp } from '@/utils/http/axios';
import {
  RegisterRespModel,
  LoginParams,
  LoginResultModel,
  GetUserInfoModel,
  SmsParams,
  ResetPasswordParams,
  SetNewPasswordParams,
} from './model/userModel';
import { ResponseModel } from '../demo/model/commonModel';

import { ErrorMessageMode } from '#/axios';
import { getAuthHeader } from './auth_header';

enum Api {
  SendingIdentifyingCode = '/user/sending_identifying_code',
  Register = '/user/register',
  ResetPassword = '/user/reset_pwd',
  SetNewPassword = '/user/set_new_pwd',
  Login = '/user/login',
  Logout = '/user/logout',
  GetPermCode = '/getPermCode',
  GetUserInfo = '/user/info',
  TestRetry = '/testRetry',
}

/**
 * @description: send sms
 */
export function sendIdentifyingCode(params: SmsParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ResponseModel>(
    {
      url: Api.SendingIdentifyingCode,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/*
 * register
 */
export function registerApi(params: SmsParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<RegisterRespModel>(
    {
      url: Api.Register,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * reset password
 */
export function resetPassword(params: ResetPasswordParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ResponseModel>(
    {
      url: Api.ResetPassword,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * set new password
*/
export function setNewPassword(params: SetNewPasswordParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<ResponseModel>(
    {
      url: Api.SetNewPassword,
      params,
      headers: getAuthHeader(),
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: user login api
 */
export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return defHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */

export function getUserInfo() {
  return defHttp.get<GetUserInfoModel>(
    { url: Api.GetUserInfo, headers: getAuthHeader() },
    { errorMessageMode: 'none' },
  );
}

export function getPermCode() {
  return defHttp.get<string[]>({ url: Api.GetPermCode });
}

export function doLogout() {
  return defHttp.get({ url: Api.Logout });
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}
