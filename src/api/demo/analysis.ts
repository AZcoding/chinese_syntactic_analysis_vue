import { defHttp } from '@/utils/http/axios';
import { AnalysisAnalyzeParams } from './model/analysisModel';
import { getAuthHeader } from '../sys/auth_header';
import { ResponseModel } from './model/commonModel';

enum Api {
  ANALYSIS_ANALYSE = '/analysis/analyse',
}

/**
 * 获取分析的状态
 */
export const analyse = (params: AnalysisAnalyzeParams) =>
  defHttp.post<ResponseModel>({
    url: Api.ANALYSIS_ANALYSE,
    params,
    headers: getAuthHeader(),
  });
