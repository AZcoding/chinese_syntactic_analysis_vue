import {
  AccountParams,
  DeptListItem,
  MenuParams,
  RoleParams,
  RolePageParams,
  MenuListGetResultModel,
  DeptListGetResultModel,
  AccountListGetResultModel,
  RolePageListGetResultModel,
  RoleListGetResultModel,
  LogListParams,
  LogListGetResultModel,
  PermissionPageParams,
  PermissionPageListGetResultModel,
  SaveRoleParams,
  SavePermissionParams,
  UserInfoParam,
  SyntaxLevelPageParams,
  SyntaxLevelPageListGetResultModel,
  SaveSyntaxParams,
  DimensionResultModel
} from './model/systemModel';
import { defHttp } from '@/utils/http/axios';
import { formatToDateTime } from '@/utils/dateUtil';
import { ResponseModel } from './model/commonModel';

enum Api {
  AccountList = '/user/list',
  RolePageList = '/role/list',
  SyntaxLevelPageList = '/syntax_level/list',
  updateUserRole = '/user/admin.update_role',
  updateUserInfo = '/user_attr/update',
  setRoleStatus = '/role/update_status',
  setUserStatus = '/user/admin.modify_status',
  LogList = '/log/list',
  SaveRole = '/role/save',
  DeleteRole = '/role/delete',
  SaveSyntaxLevel = '/syntax_level/save',
  DeleteSyntaxLevel = '/syntax_level/delete',
  DeleteSyntaxLevelRule = '/syntax_level/delete_rule',
  PermissionPageList = '/permission/list',
  SavePermission = '/permission/save',
  GrantPermission = '/permission/grant',
  RevokePermission = '/permission/revoke',
  DeletePermission = '/permission/delete',

  IsAccountExist = '/system/accountExist',
  DeptList = '/system/getDeptList',
  MenuList = '/system/getMenuList',
  GetAllRoleList = '/system/getAllRoleList',
  GetDimension = '/syntax_level/dimension'
}

export const getAccountList = (params: AccountParams) =>
  defHttp.get<AccountListGetResultModel>({ url: Api.AccountList, params }).then((res)=>{
    res.list.forEach(function (item) {
      item.register_at = formatToDateTime(new Date(Number(item.register_at) * 1000));
    });
    // 进行一层数据转换
    return {
      items: res.list,
      total: res.count,
    };
  });

export const getLogList = (params: LogListParams) =>
defHttp.get<LogListGetResultModel>({ url: Api.LogList, params }).then((res)=>{
  // 进行一层数据转换
  return {
    items: res.list,
    total: res.count,
  };
});

export const SaveRoleApi = (params: SaveRoleParams) =>
defHttp.post({ url: Api.SaveRole, params });

export const SaveSyntaxLevelApi = (params: SaveSyntaxParams) =>
defHttp.post({ url: Api.SaveSyntaxLevel, params });

export const deleteRole = (role_ids: number[]) =>
defHttp.post({ url: Api.DeleteRole, params: { role_ids:role_ids}});

export const SavePermissionApi = (list: SavePermissionParams[]) =>
defHttp.post({ url: Api.SavePermission, params:{ permission_list: list} });

export const deletePermission = (permission_ids: number[]) =>
defHttp.post({ url: Api.DeletePermission, params: { permission_ids:permission_ids}});

export const getDeptList = (params?: DeptListItem) =>
  defHttp.get<DeptListGetResultModel>({ url: Api.DeptList, params });

export const getMenuList = (params?: MenuParams) =>
  defHttp.get<MenuListGetResultModel>({ url: Api.MenuList, params });

export const getRoleListByPage = (params?: RolePageParams) =>
  defHttp.get<RolePageListGetResultModel>({ url: Api.RolePageList, params }).
  then((res)=>{
    return {
      items: res.list,
      total: res.count,
    };
  });

export const getSyntaxLvelListByPage = (params?: SyntaxLevelPageParams) =>
defHttp.get<SyntaxLevelPageListGetResultModel>({ url: Api.SyntaxLevelPageList, params }).
then((res)=>{
  if (params.call_type == 'rule') {
    return {
      items: res.list,
      total: res.list[0].rules.length,
    };
  }
  return {
    items: res.list,
    total: res.count,
  };
});

export const deleteSyntaxLevel = (ids: number[]) =>
defHttp.post({ url: Api.DeleteSyntaxLevel, params: { ids:ids}});

export const deleteSyntaxLevelRule = (ids: number[]) =>
defHttp.post({ url: Api.DeleteSyntaxLevelRule, params: { ids:ids}});

// 权限列表
export const getPermissionListByPage = (params?: PermissionPageParams) =>
defHttp.get<PermissionPageListGetResultModel>({ url: Api.PermissionPageList, params }).
then((res)=>{
  return {
    items: res.list,
    total: res.count,
  };
});

//授予权限
export const grantPermission = (role_ids:number[],permission_ids:number[]) =>
defHttp.post<ResponseModel>({ url: Api.GrantPermission, params:{ role_ids:role_ids, permission_ids:permission_ids} });

//回收权限
export const revokePermission = (role_ids:number[],permission_ids:number[]) =>
defHttp.post({ url: Api.RevokePermission, params:{ role_ids:role_ids, permission_ids:permission_ids} });

export const getAllRoleList = (params?: RoleParams) =>
  defHttp.get<RoleListGetResultModel>({ url: Api.GetAllRoleList, params });

export const setRoleStatus = (role_id: number, status: number) =>
  defHttp.post({ url: Api.setRoleStatus, params: { role_id: role_id, status:status } });

export const updateUserRole = (user_id:number,role_id: number) =>
  defHttp.post({ url: Api.updateUserRole, params: { user_id:user_id,role_id: role_id } });

export const updateUserInfo = (params: UserInfoParam) =>
  defHttp.post({ url: Api.updateUserInfo, params: params});

export const setUserStatus = (user_id: number, status: number) =>
  defHttp.post({ url: Api.setUserStatus, params: { user_id: user_id, status:status } });

export const isAccountExist = (account: string) =>
  defHttp.post({ url: Api.IsAccountExist, params: { account } }, { errorMessageMode: 'none' });

export const getDimension = () =>
  defHttp.get<DimensionResultModel>({ url: Api.GetDimension });
