import {
  BasicPageParams,
  AnalysisPageParams,
  BasicFetchResult,
  AnalysisStatusParams,
  AnalysisStatisticParams,
} from '@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type DemoParams = Partial<BasicPageParams>;

export interface DemoListItem {
  id: string;
  beginTime: string;
  endTime: string;
  address: string;
  name: string;
  no: number;
  status: number;
}

/**
 * @description: Request list return value
 */
export type DemoListGetResultModel = BasicFetchResult<DemoListItem>;

/**
 * 分析列表
 */
/**
 * @description: Request list interface parameters
 */
export type AnalysisListParams = Partial<AnalysisPageParams>;

export interface AnalysisListItem {
  request_id: string;
  begin_time: string;
  end_time: string;
  user_id: number;
  text: string;
  result: any;
  status: number;
  status_name: string;
  level: string;
  advice: string;
}


/**
 * @description: Request list return value
 */
export type AnalysisListResultModel = BasicFetchResult<AnalysisListItem>;


/**
 * 分析统计
*/
export type GetAnalysisStatisticParams = Partial<AnalysisStatisticParams>;

export interface AnalysisStatisticResultModel {
  top_word: TopWordItem[],
  syntax_level: PieItem[],
};

export interface TopWordItem {
  name: string;
  doc_num: number;
  exist_num: number;
}

export interface PieItem {
  name: string;
  value: number;
}

/**
 * 分析状态
 */

export type GetAnalysisStatusParams = Partial<AnalysisStatusParams>;

export interface AnalysisStatusItem {
  request_id: string;
  status: number;
  status_name: string;
}

/**
 * @description: Request list return value
 */
export type AnalysisStatusResultModel = BasicFetchResult<AnalysisStatusItem>;

/**
 * 下载分析报告
 */
export interface DownloadReportParams{
  request_id: string;
  file_type: number;
  token: string;
}