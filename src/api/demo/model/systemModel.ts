import { BasicPageParams, BasicFetchResult } from '@/api/model/baseModel';

export type AccountParams = BasicPageParams & {
  user_id: number;
  status: number;
  user_name: string;
  email: string;
  sex: number;
  age: number;
};

export type LogListParams = BasicPageParams & {
  topic_name: string;
  log_content: string;
  begin_at: number;
  end_at: number;
};

export type RoleParams = {
  status: number;
  role_name?: string;
  comment?: string;
};

export type SyntaxLevelParams = {
  call_type: string;
  name: string;
};

export type UserInfoParam = {
  username: string;
  avatar: string;
  sex: number;
  age: number;
}

export type SaveRoleParams = {
  role_id: number;
  role_name: string;
  status: number;
  comment: string;
}

export type SaveSyntaxParams = {
  id: number;
  name: string;
  advice: string;
  comment: string;
  rules: SyntaxRuleItem[],
}

export interface SyntaxRuleItem {
  id: number;
  analysis_type: number;
  min_score: number;
  max_score: number;
  comment: string;
}

export type RolePageParams = BasicPageParams & RoleParams;

export type SyntaxLevelPageParams = BasicPageParams & SyntaxLevelParams;

export type SavePermissionParams = {
  permission_id: number;
  permission_name: string;
  comment: string;
  permission_type: number;
  permission_value: string;
}

export type PermissionParams = {
  permission_name?: string;
  permission_type?: number;
  permission_value?: string;
  comment?: string;
  role_id?: number;
};

export type PermissionPageParams = BasicPageParams & PermissionParams;

export type DeptParams = {
  role_name?: string;
  comment?: string;
};

export type MenuParams = {
  menuName?: string;
  status?: string;
};

export interface AccountListItem {
  user_id: number;
  status: number;
  username : string;
  role: string;
  email : string;
  sex: number;
  sex_name: string;
  age: number;
  avatar: string;
  register_at: string;
}

export interface DeptListItem {
  id: string;
  orderNo: string;
  createTime: string;
  remark: string;
  status: number;
}

export interface MenuListItem {
  id: string;
  orderNo: string;
  createTime: string;
  status: number;
  icon: string;
  component: string;
  permission: string;
}

export interface RoleListItem {
  role_id: number;
  role_name: string;
  comment: string;
  status: number;
  created_at: number;
  updated_at: number;
}

export interface DimensionItem {
  id: number,
  name: string,
}

export interface SyntaxLevelListItem {
  id: number;
  name: string;
  advice: string;
  comment: string;
  rules: SyntaxRuleItem[];
}


export interface SyntaxRuleItem {
  id: number;
  analysis_type: number;
  min_score: number;
  max_score: number;
  comment: string;
}

export interface LogListItem {
  id: number;
  topic_name: string;
  log_content: string;
  created_at: number;
}

export interface PermissionListItem {
  permission_id: number;
  permission_name: string;
  permission_type: string;
  permission_value: string;
  comment: string;
  created_at: number;
  updated_at: number;
}

/**
 * @description: Request list return value
 */
export type AccountListGetResultModel = BasicFetchResult<AccountListItem>;

export type LogListGetResultModel = BasicFetchResult<LogListItem>;

export type DeptListGetResultModel = BasicFetchResult<DeptListItem>;

export type MenuListGetResultModel = BasicFetchResult<MenuListItem>;

export type RolePageListGetResultModel = BasicFetchResult<RoleListItem>;

export type SyntaxLevelPageListGetResultModel = BasicFetchResult<SyntaxLevelListItem>;

export type PermissionPageListGetResultModel = BasicFetchResult<PermissionListItem>;

export type RoleListGetResultModel = RoleListItem[];

export type DimensionResultModel = DimensionItem[];
