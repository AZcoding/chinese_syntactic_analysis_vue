/**
 * 公共响应
 */
export interface ResponseModel {
  code: number;
  data: any;
  msg: string;
}
