export interface AnalysisAnalyzeParams {
  text: string;
  comment: string;
}
