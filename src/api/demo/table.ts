import { defHttp } from '@/utils/http/axios';
import {
  DemoParams,
  DemoListGetResultModel,
  AnalysisListParams,
  AnalysisListResultModel,
  GetAnalysisStatusParams,
  AnalysisStatusResultModel,
  DownloadReportParams,
  GetAnalysisStatisticParams,
  AnalysisStatisticResultModel,
} from './model/tableModel';
import { getAuthHeader,getAuthToken } from '../sys/auth_header';
import { formatToDateTime } from '@/utils/dateUtil';

import { getAppEnvConfig } from '@/utils/env';
import { TOKEN_KEY } from '@/enums/cacheEnum';
import { WebStorage } from '@/utils/cache/index';
import axios from 'axios';
import { useUserStore } from '@/store/modules/user';

const userStore = useUserStore();

const {  VITE_GLOB_API_URL } =
    getAppEnvConfig();

enum Api {
  DEMO_LIST = '/table/getDemoList',
  ALL_ANALYSIS_LIST = '/analysis/get_all_analysis_list',
  SELF_ANALYSIS_LIST = '/analysis/get_self_analysis_list',
  ANALYSIS_STATISTIC = '/analysis/statistic',
  ANALYSIS_STATUS = '/analysis/check_analysis_status',
  DOWNLOAD_ANALYSIS_REPORT = '/analysis/download_report',
  DOWNLOAD_ANALYSIS_TEMPLATE = "/analysis/download_analysis_template"
}

const analysisStatusMap = new Map([
  [1, '进行中'],
  [2, '已完成'],
  [3, '异常'],
]);

/**
 * @description: Get sample list value
 */

export const demoListApi = (params: DemoParams) =>
  defHttp.get<DemoListGetResultModel>({
    url: Api.DEMO_LIST,
    params,
    headers: {
      // @ts-ignore
      ignoreCancelToken: true,
    },
  });

/**
 * 获取分析列表
 */

export const getAnalysisList = (params: AnalysisListParams) =>
{
  let url = Api.SELF_ANALYSIS_LIST;
  if (params.api_type == 'all') {
    url = Api.ALL_ANALYSIS_LIST;
  }
  
  return defHttp
  .get<AnalysisListResultModel>({
    url: url,
    params,
    headers: getAuthHeader(),
  })
  .then((res) => {
    // 做一层数据格式转换
    // console.log("res",res);
    res.list.forEach(function (item) {
      item.begin_time = formatToDateTime(new Date(Number(item.begin_time) * 1000));
      item.end_time = formatToDateTime(new Date(Number(item.end_time) * 1000));
      const status_name = analysisStatusMap.get(item.status);
      if (status_name != undefined) {
        item.status_name = status_name;
      }
    });
    return {
      items: res.list,
      total: res.count,
    };
  })
};


/**
 * 获取统计数据
*/
// export const getAnalysisStatistic = (params: GetAnalysisStatisticParams) =>
//   defHttp
//     .post<AnalysisStatisticResultModel>({
//       url: Api.ANALYSIS_STATISTIC,
//       params,
//       headers: getAuthHeader(),
//     });

export const getAnalysisStatistic = (params: GetAnalysisStatisticParams) =>
  defHttp
    .post({
      url: Api.ANALYSIS_STATISTIC,
      params,
      headers: getAuthHeader(),
    });

/**
 * 获取分析的状态
 */
export const getAnalysisStatus = (params: GetAnalysisStatusParams) =>
  defHttp
    .post<AnalysisStatusResultModel>({
      url: Api.ANALYSIS_STATUS,
      params,
      headers: getAuthHeader(),
    })
    .then((res) => {
      // 做一层数据格式转换
      // console.log("res",res);
      res.forEach(function (item) {
        const status_name = analysisStatusMap.get(item.status);
        if (status_name != undefined) {
          item.status_name = status_name;
        }
      });
      return {
        items: res,
        total: 0,
      };
    });

// 下载分析报告
export const downloadAnalysisReport = (params: DownloadReportParams) =>
{
    // defHttp.post({
  //   url: Api.DOWNLOAD_ANALYSIS_REPORT,
  //   params,
  //   headers: getAuthHeader(),
  // })
    axios({
      method: "post",
      headers: {'Access-Token':userStore.getToken},
      url:  VITE_GLOB_API_URL + Api.DOWNLOAD_ANALYSIS_REPORT,
      // params: params, // query
      data: params, // 请求体
      responseType: "blob",
    }).then(function (res) {
    let blob = new Blob([res.data]); // { type: "application/vnd.ms-excel" }
    let url = window.URL.createObjectURL(blob); // 创建一个临时的url指向blob对象
    // 创建url之后可以模拟对此文件对象的一系列操作，例如：预览、下载
    let a = document.createElement("a");
    a.href = url;
    a.download = "分析报告-"+String(new Date().getTime())+".pdf";
    a.click();
    // 释放这个临时的对象url
    window.URL.revokeObjectURL(url);
  }).catch((err) =>{
    console.log(err);
  })
}

// 下载分析模板
export const downloadAnalysisTemplate = () =>
{

    axios({
      method: "get",
      headers: {'Access-Token':userStore.getToken},
      url:  VITE_GLOB_API_URL + Api.DOWNLOAD_ANALYSIS_TEMPLATE,
      responseType: "blob",
    }).then(function (res) {
    let blob = new Blob([res.data]); // { type: "application/vnd.ms-excel" }
    let url = window.URL.createObjectURL(blob); // 创建一个临时的url指向blob对象
    // 创建url之后可以模拟对此文件对象的一系列操作，例如：预览、下载
    let a = document.createElement("a");
    a.href = url;
    a.download = "批量分析模板.xlsx";
    a.click();
    // 释放这个临时的对象url
    window.URL.revokeObjectURL(url);
  }).catch((err) =>{
    console.log(err);
  })
}
